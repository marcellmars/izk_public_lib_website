<!--
.. title: The opening
.. author:
    - collective
.. slug: the-opening
.. date: 2017-06-02 11:29:50 UTC+02:00
.. tags: 
.. category: 
.. link: 
.. description:
-->


We built the library together. Here it is:

* [izk bookshelf catalog](http://public.lib/catalogs/izk_catalog/BROWSE_LIBRARY.html)
* [lecture notes](http://public.lib/catalogs/lecture_notes/BROWSE_LIBRARY.html)
* [izk digital collection](http://public.lib/catalogs/izk_digital_collection/BROWSE_LIBRARY.html)
* [book scanning manual](http://public.lib/files/scanning_manual.pdf)


* [our etherpad notes](http://pad.constantvzw.org/p/letsbuildalibrarytogether.notes)
* [whatsapp public.lib group chat](https://chat.whatsapp.com/FDnzeUw4k6C6uBNQsqzohj)
